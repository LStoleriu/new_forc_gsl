// _NEW_FORC.cpp : Defines the entry point for the console application.
//

//#include "stdafx.h"

#include  <stdio.h>
#include  <math.h>
#include  <stdlib.h>
#include  <string.h>

#include <gsl/gsl_bspline.h>
#include <gsl/gsl_multifit.h>


#include <gsl/gsl_statistics.h>

//#include "imsl.h"
//#include "imsls.h"


//#define NOBS1           1000
//#define NOBS            NOBS1
//#define NFORC			27
//#define NMAX_PER_FORC   115

int NOBS1, NOBS, NFORC, NMAX_PER_FORC;


#define N_INDEP			5
#define N_COEFS			N_INDEP + 1

#define SP				2

#define alpha			0.00

#define SMOOTH_TEST 1
//#undef SMOOTH_TEST

#define SMOOTH 1
#undef SMOOTH

#define PRELUNGIRE 1
#undef PRELUNGIRE
#define PRELUNGIRE_CONST 1
#define PRELUNGIRE_SYM 1

#define CLASSIC 1
#undef CLASSIC

#define LOESS 1
#undef LOESS

#define LOESS_ON_ORIGIAL_DATA 1
#undef LOESS_ON_ORIGIAL_DATA

// double H[NOBS], Hr[NOBS], M[NOBS];
// double H_pre[NFORC*NMAX_PER_FORC], Hr_pre[NFORC*NMAX_PER_FORC], M_pre[NFORC*NMAX_PER_FORC];
// double H_p[NFORC*NFORC], Hr_p[NFORC*NFORC], M_p[NFORC*NFORC];
// 
// double w[NOBS], xx[NOBS*N_INDEP], dist[NOBS];
// int permutations[NOBS];
// double w_p[NFORC*NFORC], xx_p[NFORC*NFORC*N_INDEP], dist_p[NFORC*NFORC];
// int *permutations_p[NFORC*NFORC];
// int Nr_pct_per_FORC[NFORC];

double *H, *Hr, *M, *H_pre, *Hr_pre, *M_pre, *H_p, *Hr_p, *M_p;
double *w, *xx, *dist, *w_p, *xx_p, *dist_p;
int *Nr_pct_per_FORC;

double aa[(2 * SP + 1)*(2 * SP + 1)][6];
double bb[(2 * SP + 1)*(2 * SP + 1)];

double dmax;

double W(double x);
double *sorted_result, *coeffs;
int rank, wrong = 0;
double  tol = 1.0e-4;



int main(void)
{
	long i, j, k, l, contor;

	FILE *fp, *fp45, *fp_FORCcolo;
	int     *kbasis;

	//imsls_error_options(IMSLS_SET_PRINT, IMSLS_WARNING, 0, 0);


	// 	char nume_fisIN[500]					= "E:\\Stoleriu\\C\\special\\3d\\res\\2016\\LLGx2\\LLGx2_200x200_exch_0.1-rK0.25-HardOnly-Fin\\PlusPara\\1000_CUBes_FORC - Copy.dat";
	// 	char nume_fisOUT_SMOOTH[500]			= "E:\\Stoleriu\\C\\special\\3d\\res\\2016\\LLGx2\\LLGx2_200x200_exch_0.1-rK0.25-HardOnly-Fin\\PlusPara\\1000_CUBes_FORC.SMOOTHtest.dat";
	// 	char nume_fisOUT_DRAW[500]				= "E:\\Stoleriu\\C\\special\\3d\\res\\2016\\LLGx2\\LLGx2_200x200_exch_0.1-rK0.25-HardOnly-Fin\\PlusPara\\1000_CUBes_FORC.drawSMOOTH.dat";
	// 	char nume_fisOUT_DRAW_regular[500]		= "E:\\Stoleriu\\C\\special\\3d\\res\\2016\\LLGx2\\LLGx2_200x200_exch_0.1-rK0.25-HardOnly-Fin\\PlusPara\\1000_CUBes_FORC.DRAW.dat";	
	// 	char nume_fisOUT_PREbrut[500]			= "E:\\Stoleriu\\C\\special\\3d\\res\\2016\\LLGx2\\LLGx2_200x200_exch_0.1-rK0.25-HardOnly-Fin\\PlusPara\\1000_CUBes_FORC.PRELUNGIT.BRUT.dat";
	// 	char nume_fisOUT_PREspln[500]			= "E:\\Stoleriu\\C\\special\\3d\\res\\2016\\LLGx2\\LLGx2_200x200_exch_0.1-rK0.25-HardOnly-Fin\\PlusPara\\1000_CUBes_FORC.PRELUNGIT.SPLINE.dat";
	// 	char nume_fisOUT_WRKSHT[500]			= "E:\\Stoleriu\\C\\special\\3d\\res\\2016\\LLGx2\\LLGx2_200x200_exch_0.1-rK0.25-HardOnly-Fin\\PlusPara\\1000_CUBes_FORC.REGULAR.wrksht";
	// 	char nume_fisOUT_LOESS[500]				= "E:\\Stoleriu\\C\\special\\3d\\res\\2016\\LLGx2\\LLGx2_200x200_exch_0.1-rK0.25-HardOnly-Fin\\PlusPara\\1000_CUBes_FORC.LOESS.dat";
	// 	char nume_fisOUT_LOESS45[500]			= "E:\\Stoleriu\\C\\special\\3d\\res\\2016\\LLGx2\\LLGx2_200x200_exch_0.1-rK0.25-HardOnly-Fin\\PlusPara\\1000_CUBes_FORC.LOESS45.dat";

	char cale[1000] = "E:\\Stoleriu\\C\\special\\3d\\res\\2017\\FORC-GSL-test\\FORC_2400_M_sixlayers_para";
	char nume_fisIN[1000];
	char nume_fisOUT_SMOOTH[1000];
	char nume_fisOUT_DRAW[1000];
	char nume_fisOUT_DRAW_regular[1000];
	char nume_fisOUT_DRAW_colo[1000];
	char nume_fisOUT_PREbrut[1000];
	char nume_fisOUT_PREspln[1000];
	char nume_fisOUT_WRKSHT[1000];
	char nume_fisOUT_LOESS[1000];
	char nume_fisOUT_LOESS45[1000];

	double q = 0.025;

	//////////////////////////////////////////////////////////////////////////
	/////// ANALIZA FISIER
	//////////////////////////////////////////////////////////////////////////
	double dummy;
	strcpy(nume_fisIN, cale);
	strcat(nume_fisIN, ".dat");
	strcpy(nume_fisOUT_SMOOTH, cale);
	strcat(nume_fisOUT_SMOOTH, ".SMOOTHtest.dat");
	strcpy(nume_fisOUT_DRAW, cale);
	strcat(nume_fisOUT_DRAW, ".drawSMOOTH.dat");
	strcpy(nume_fisOUT_DRAW_regular, cale);
	strcat(nume_fisOUT_DRAW_regular, ".DRAW.dat");
	strcpy(nume_fisOUT_DRAW_colo, cale);
	strcat(nume_fisOUT_DRAW_colo, ".drawCOLO.dat");
	strcpy(nume_fisOUT_PREbrut, cale);
	strcat(nume_fisOUT_PREbrut, ".SMOOTHtest.dat");
	strcpy(nume_fisOUT_PREbrut, cale);
	strcat(nume_fisOUT_PREbrut, ".PRELUNGIT.BRUT.dat");
	strcpy(nume_fisOUT_PREspln, cale);
	strcat(nume_fisOUT_PREspln, ".PRELUNGIT.SPLINE.dat");
	strcpy(nume_fisOUT_WRKSHT, cale);
	strcat(nume_fisOUT_WRKSHT, ".REGULAR.wrksht.dat");
	strcpy(nume_fisOUT_LOESS, cale);
	strcat(nume_fisOUT_LOESS, ".LOESS.dat");
	strcpy(nume_fisOUT_LOESS45, cale);
	strcat(nume_fisOUT_LOESS45, ".LOESS45.dat");

	fopen_s(&fp, nume_fisIN, "r");
	{
		int n = 0;
		int N = 0;
		int np = 0;
		int nF = 1;

		double Hr_temp, H_temp, M_temp;
		double Hr_last, dummy;

		while (fscanf_s(fp, "%lf %lf %lf \n", &Hr_temp, &H_temp, &M_temp) != EOF)
			//while (fscanf_s(fp, "%lf %lf %lf %lf\n", &Hr_temp, &H_temp, &M_temp, &dummy) != EOF)
		{
			//M_temp += dummy;
			if (np == 0)
				Hr_last = Hr_temp;

			np++;
			N++;

			if ((n == 0) && (Hr_last != Hr_temp))
			{
				n = np - 1;
				nF++;
				Hr_last = Hr_temp;
			}
			else
			{
				if ((Hr_last != Hr_temp))
				{
					N = 1;
					nF++;
					Hr_last = Hr_temp;
				}
			}
		}
		fclose(fp);
		NOBS = np;
		NOBS1 = NOBS;
		NMAX_PER_FORC = N;
		NFORC = nF;
	}

	printf("........... %d puncte .............\n", NOBS);
	printf("............ %d curbe  ..............\n", NFORC);
	printf("...... %d max puncte pe curba  ......\n\n", NMAX_PER_FORC);

	printf("............. SP = %d ................\n", SP);
	printf("...... suggested q = %6.4lf .........\n\n", (double)(2 * SP + 1) / NOBS);

	int NDATE = floor(NOBS * q + 0.5);

	H = (double *)calloc(NOBS, sizeof(double));
	Hr = (double *)calloc(NOBS, sizeof(double));
	M = (double *)calloc(NOBS, sizeof(double));

	H_pre = (double *)calloc(NFORC*NMAX_PER_FORC, sizeof(double));
	Hr_pre = (double *)calloc(NFORC*NMAX_PER_FORC, sizeof(double));
	M_pre = (double *)calloc(NFORC*NMAX_PER_FORC, sizeof(double));

	H_p = (double *)calloc(NFORC*NFORC, sizeof(double));
	Hr_p = (double *)calloc(NFORC*NFORC, sizeof(double));
	M_p = (double *)calloc(NFORC*NFORC, sizeof(double));

	w = (double *)calloc(NFORC*NMAX_PER_FORC, sizeof(double));
	xx = (double *)calloc(NFORC*NMAX_PER_FORC*N_INDEP, sizeof(double));
	dist = (double *)calloc(NFORC*NMAX_PER_FORC, sizeof(double));

	w_p = (double *)calloc(NFORC*NFORC, sizeof(double));
	xx_p = (double *)calloc(NFORC*NFORC*N_INDEP, sizeof(double));
	dist_p = (double *)calloc(NFORC*NFORC, sizeof(double));


	Nr_pct_per_FORC = (int *)calloc(NFORC, sizeof(int));

	//double M_SMOOTH[NOBS];
	//double xcoord[NMAX_PER_FORC], ycoord[NMAX_PER_FORC];
	double *M_SMOOTH, *xcoord, *ycoord;
	M_SMOOTH = (double *)calloc(NOBS, sizeof(double));
	xcoord = (double *)calloc(NMAX_PER_FORC, sizeof(double));
	ycoord = (double *)calloc(NMAX_PER_FORC, sizeof(double));

	if ((H == NULL) || (Hr == NULL) || (M == NULL) || (H_pre == NULL) || (Hr_pre == NULL) || (M_pre == NULL) || (H_p == NULL) || (Hr_p == NULL) || (M_p == NULL) || (w == NULL) || (xx == NULL) || (dist == NULL) || (w_p == NULL) || (xx_p == NULL) || (dist_p == NULL) || (Nr_pct_per_FORC == NULL) || (M_SMOOTH == NULL) || (xcoord == NULL) || (ycoord == NULL))
	{
		printf("CALLOC ERROR!!!!\n");
		exit(1);
	}

	double H_norm = 0.0;
	double M_norm = 0.0;

	fopen_s(&fp, nume_fisIN, "r");
	for (i = 0;i < NOBS1;i++)
	{
		fscanf_s(fp, "%lf  %lf  %lf \n", &Hr[i], &H[i], &M[i]);
		//fscanf_s(fp, "%lf  %lf  %lf %lf\n", &Hr[i], &H[i], &M[i],  &dummy);
		//M[i] += dummy;
		if (H[i] > H_norm)
			H_norm = H[i];
		if (Hr[i] > H_norm)
			H_norm = Hr[i];
		if (M[i] > M_norm)
			M_norm = M[i];
	}
	fclose(fp);

	////////////////////////////////////////////////////////////////////////////
	////// NORM
	////////////////////////////////////////////////////////////////////////////

	for (i = 0;i < NOBS1;i++)
	{
		Hr[i] /= H_norm;
		H[i] /= H_norm;
		M[i] /= M_norm;
	}

	////////////////////////////////////////////////////////////////////////////
	////// MOVING
	////////////////////////////////////////////////////////////////////////////
	double mom_to_remember = M[0];
	double classic_Hr;
	classic_Hr = Hr[0];
	Hr[0] += alpha*mom_to_remember;
	H[0] += alpha*M[0];
	for (i = 1;i < NOBS;i++)
	{
		if (Hr[i] != classic_Hr)
		{
			mom_to_remember = M[i];
		}
		classic_Hr = Hr[i];
		Hr[i] += alpha*mom_to_remember;
		H[i] += alpha*M[i];
	}

	////////////////////////////////////////////////////////////////////////////
	////// SMOOTHING TEST
	////////////////////////////////////////////////////////////////////////////
#ifdef SMOOTH_TEST
#define prim_pas_test  ((NOBS)-NMAX_PER_FORC)
#define last_pas_test  (NOBS)
	int index1;
	double *sp_tst = NULL;

	const size_t n = (last_pas_test - prim_pas_test);
	const size_t ncoeffs = n/2;
	const size_t grad = 4;
	const size_t nbreak = (ncoeffs + 2 - grad);

	gsl_bspline_workspace *bw;
	gsl_vector *B;
	double dy;

	gsl_vector *c, *w;
	gsl_matrix *X, *cov;
	gsl_multifit_linear_workspace *mw;

	double chisq, Rsq, dof, tss;

	
	gsl_vector * ics, *igrec;


	bw = gsl_bspline_alloc(4, nbreak);
	B = gsl_vector_alloc(ncoeffs);

	ics = gsl_vector_alloc(n);
	igrec = gsl_vector_alloc(n);
	X = gsl_matrix_alloc(n, ncoeffs);
	c = gsl_vector_alloc(ncoeffs);
	w = gsl_vector_alloc(n);
	cov = gsl_matrix_alloc(ncoeffs, ncoeffs);
	mw = gsl_multifit_linear_alloc(n, ncoeffs);

	printf("SMOOTH TEST\n\n");

	/* this is the data to be fitted */
	for (i = 0; i < n; ++i)
	{
		double sigma;
		index1 = i + (prim_pas_test);
		sigma = 0.1 * M[index1];

		gsl_vector_set(ics, i, H[index1]);
		gsl_vector_set(igrec, i, M[index1]);
		gsl_vector_set(w, i, 1.0 / (sigma * sigma));
	}

	/* use uniform breakpoints on [0, 15] */
	gsl_bspline_knots_uniform(H[prim_pas_test], H[last_pas_test-1], bw);


	/* construct the fit matrix X */
	for (i = 0; i < n; ++i)
	{
		double xi = gsl_vector_get(ics, i);

		/* compute B_j(xi) for all j */
		gsl_bspline_eval(xi, B, bw);

		/* fill in row i of X */
		for (j = 0; j < ncoeffs; ++j)
		{
			double Bj = gsl_vector_get(B, j);
			gsl_matrix_set(X, i, j, Bj);
		}
	}

	/* do the fit */
	gsl_multifit_wlinear(X, w, igrec, c, cov, &chisq, mw);

	dof = n - ncoeffs;
	tss = gsl_stats_wtss(w->data, 1, igrec->data, 1, igrec->size);
	Rsq = 1.0 - chisq / tss;

	//fprintf(stderr, "chisq/dof = %e, Rsq = %f\n", chisq / dof, Rsq);

	printf("\n\n");

	/* output the smoothed curve */

	double xi, yi, yerr;
	fopen_s(&fp, nume_fisOUT_SMOOTH, "w");
	for (i = 0; i < n; i++)
	{
		xi = gsl_vector_get(ics, i);
		gsl_bspline_eval(xi, B, bw);
		gsl_multifit_linear_est(B, c, cov, &yi, &yerr);
		printf("%lf %lf %lf\n", gsl_vector_get(ics, i), gsl_vector_get(igrec, i), yi);
		fprintf(fp, "%lf %lf %lf\n", gsl_vector_get(ics, i), gsl_vector_get(igrec, i), yi);
	}
	fclose(fp);


	gsl_bspline_free(bw);
	gsl_vector_free(B);
	gsl_vector_free(ics);
	gsl_vector_free(igrec);
	gsl_matrix_free(X);
	gsl_vector_free(c);
	gsl_vector_free(w);
	gsl_matrix_free(cov);
	gsl_multifit_linear_free(mw);
#endif
	//exit(1);
	////////////////////////////////////////////////////////////////////////////
	////// SMOOTHING FOR REAL
	////////////////////////////////////////////////////////////////////////////
#ifdef SMOOTH
	double *smth;
	Imsl_d_ppoly        *spline;

	contor = 1;
	int contor_FORC = 0, contor_SMOOTH_FORC = 0;

	printf("SMOOOOOOOOOOOOOTH\n\n");

	for (i = 1; i < NOBS; i++)		//////////////////// numarare pct. pe fiecare FORC
	{
		if (Hr[i] != Hr[i - 1])
		{
			Nr_pct_per_FORC[contor_FORC] = contor;
			contor = 1;
			contor_FORC++;
		}
		else
		{
			contor++;
		}
	}
	Nr_pct_per_FORC[NFORC - 1] = contor;

	fopen_s(&fp, nume_fisOUT_DRAW, "w");
	contor_FORC = 0;
	contor_SMOOTH_FORC = 0;
	for (j = 0; j < NFORC; j++)
	{
		for (i = 0; i < Nr_pct_per_FORC[j]; i++)
		{
			xcoord[i] = H[contor_FORC];
			ycoord[i] = M[contor_FORC];
			contor_FORC++;
		}

		if (Nr_pct_per_FORC[j] >= 7)
			smth = imsl_d_smooth_1d_data(Nr_pct_per_FORC[j], xcoord, ycoord, IMSL_DISTANCE, 1.0, IMSL_STOPPING_CRITERION, 0.2, IMSL_ITMAX, 10000, 0);
		//spline = imsl_d_cub_spline_smooth(Nr_pct_per_FORC[j], xcoord, ycoord, IMSL_SMOOTHING_PAR, 0.01, 0);


		for (i = 0; i < Nr_pct_per_FORC[j]; i++)
		{
			if (Nr_pct_per_FORC[j] >= 7)
				M_SMOOTH[contor_SMOOTH_FORC] = /*imsl_d_cub_spline_value(xcoord[i], spline, 0);*/smth[i];
			else
				M_SMOOTH[contor_SMOOTH_FORC] = M[contor_SMOOTH_FORC];
			contor_SMOOTH_FORC++;

			fprintf(fp, "%lf %lf\n", H[contor_SMOOTH_FORC - 1] * H_norm, M_SMOOTH[contor_SMOOTH_FORC - 1] * M_norm);
		}
		fprintf(fp, "\n");
	}
	fclose(fp);

#endif
	////////////////////////////////////////////////////////////////////////////
	////// PRELUNGIRE
	////////////////////////////////////////////////////////////////////////////
#ifdef PRELUNGIRE
	{
		contor = 1;
		int contor_FORC = 0;
		for (i = 1; i < NOBS1; i++)		//////////////////// numarare pct. pe fiecare FORC
		{
			if (Hr[i] != Hr[i - 1])
			{
				Nr_pct_per_FORC[contor_FORC] = contor;
				contor = 1;
				contor_FORC++;
			}
			else
			{
				contor++;
			}
		}
		Nr_pct_per_FORC[NFORC - 1] = contor;

		//////////////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////////////// BEGIN prelungire CONST.
		contor = 0;
		for (i = 0; i < NFORC; i++)
		{
			for (j = 0; j < Nr_pct_per_FORC[NFORC - 1]; j++) //de la 0 la numarul maxim de puncte pe FORC
			{
				if (j < (Nr_pct_per_FORC[NFORC - 1] - Nr_pct_per_FORC[i])) //de la 0 la (nr maxim) - (nr pct ale FORC curent)
				{
					Hr_pre[i*NMAX_PER_FORC + j] = Hr[contor];
					H_pre[i*NMAX_PER_FORC + j] = H[NOBS1 - Nr_pct_per_FORC[NFORC - 1] + j];//H luate din ultimul FORC
#ifdef SMOOTH
					M_pre[i*NMAX_PER_FORC + j] = M_SMOOTH[contor];
#else // SMOOTH
					M_pre[i*NMAX_PER_FORC + j] = M[contor];
#endif //SMOOTH
				}
				else
				{
					Hr_pre[i*NMAX_PER_FORC + j] = Hr[contor];
					H_pre[i*NMAX_PER_FORC + j] = H[contor];
#ifdef SMOOTH
					M_pre[i*NMAX_PER_FORC + j] = M_SMOOTH[contor];
#else // SMOOTH
					M_pre[i*NMAX_PER_FORC + j] = M[contor];
#endif //SMOOTH
					contor++;
				}
			}
		}

		fopen_s(&fp, nume_fisOUT_PREbrut, "w");
		for (i = 0; i < NFORC*NMAX_PER_FORC; i++)
		{
			fprintf_s(fp, "%20.16lf  %20.16lf  %20.16lf\n", Hr_pre[i] * H_norm, H_pre[i] * H_norm, M_pre[i] * M_norm);
		}
		fclose(fp);
		////////////////////////////////////////////////////////////////////////// END prelungire CONST.
		//////////////////////////////////////////////////////////////////////////

		////////////////////////////////////////////////////////////////////////// BEGIN interpolare
		double *xdata/*[NMAX_PER_FORC]*/, *fdata/*[NMAX_PER_FORC]*/, H_interp, M_interp, last_M;
		xdata = (double *)calloc(NMAX_PER_FORC, sizeof(double));
		fdata = (double *)calloc(NMAX_PER_FORC, sizeof(double));

		if ((xdata == NULL) || (fdata == NULL))
		{
			printf("CALLOC ERROR in SPLINE!!! \n\n");
			exit(1);
		}

		Imsl_d_spline *sp;

		contor = 0;
		for (i = 0; i < NFORC; i++)
		{
			for (j = 0; j < NFORC; j++)
			{
				Hr_p[i*NFORC + j] = Hr_pre[i*NMAX_PER_FORC];
				H_p[i*NFORC + j] = Hr_pre[j*NMAX_PER_FORC];
				M_p[i*NFORC + j] = M_pre[0];
			}
		}

		for (i = 1; i < NFORC; i++)											// numarul FORC-ului
		{
			for (j = NMAX_PER_FORC - Nr_pct_per_FORC[i]; j < NMAX_PER_FORC; j++)
			{
				xdata[j - NMAX_PER_FORC + Nr_pct_per_FORC[i]] = H_pre[i*NMAX_PER_FORC + j];
				fdata[j - NMAX_PER_FORC + Nr_pct_per_FORC[i]] = M_pre[i*NMAX_PER_FORC + j];
			}

			if (Nr_pct_per_FORC[i] > 3)
			{
				//printf("%d  %lf\n", i, xdata[0]);
				sp = imsl_d_spline_interp(Nr_pct_per_FORC[i], xdata, fdata, 0);

				for (j = 0; j < NFORC; j++)
				{
					H_interp = H_p[i*NFORC + j];

					if (H_interp >= xdata[0]) //avem de ce interpola
					{
						M_interp = imsl_d_spline_value(H_interp, sp, 0);
						M_p[i*NFORC + j] = M_interp;
						last_M = M_interp;
					}
					else
					{
						M_p[i*NFORC + j] = last_M;
					}
				}
			}
		}

		fopen_s(&fp, nume_fisOUT_PREspln, "w");
		for (i = 0; i < NFORC*NFORC; i++)
		{
			fprintf_s(fp, "%20.16lf  %20.16lf  %20.16lf\n", Hr_p[i] * H_norm, H_p[i] * H_norm, M_p[i] * M_norm);
		}
		fclose(fp);
		printf("NFORC=%d, NOBS=%d\n", NFORC, NFORC*NMAX_PER_FORC);

		fopen_s(&fp, nume_fisOUT_DRAW_regular, "w");
		for (i = 0; i < NFORC; i++)
		{
			for (j = 0; j <= i; j++)
			{
				fprintf_s(fp, "%20.16lf  %20.16lf\n", H_p[i*NFORC + j] * H_norm, M_p[i*NFORC + j] * M_norm);
			}
			fprintf_s(fp, "\n");
		}
		fclose(fp);

		//printf("Salvat fisierul prelungit\n\nEXITING!\n");
		//getchar();
		//exit(1);

		free(xdata); free(fdata);
	}
#endif
	////////////////////////////////////////////////////////////////////////////
	////// FORC CLASSIC
	////////////////////////////////////////////////////////////////////////////
#ifdef CLASSIC
	{
		fopen_s(&fp, nume_fisOUT_WRKSHT, "w");
		fopen_s(&fp_FORCcolo, nume_fisOUT_DRAW_colo, "w");

#ifndef PRELUNGIRE
		{
			int *loco_loco_perm_classic = NULL;
			int **loco_perm_classic = &(loco_loco_perm_classic);

			for (int j = 0; j < NOBS; j++)
			{
				/* Find points */
				for (i = 0; i < NOBS; i++)
				{
					dist[i] = sqrt((H[j] - H[i])*(H[j] - H[i]) + (Hr[j] - Hr[i])*(Hr[j] - Hr[i]));
				}
				imsl_d_sort(NOBS, &dist[0], IMSL_ABSOLUTE, IMSL_PERMUTATION, loco_perm_classic, 0);

				/* indicii sunt in permutations[0][0] ... permutations[0][NDATE-1] */
				/* se considera N puncte vecine */
				for (i = 0; i < (2 * SP + 1)*(2 * SP + 1); i++)
				{
					bb[i] = M[loco_perm_classic[0][i]];
					aa[i][0] = Hr[loco_perm_classic[0][i]];
					aa[i][1] = H[loco_perm_classic[0][i]];
					aa[i][2] = Hr[loco_perm_classic[0][i]] * Hr[loco_perm_classic[0][i]];
					aa[i][3] = H[loco_perm_classic[0][i]] * H[loco_perm_classic[0][i]];
					aa[i][4] = H[loco_perm_classic[0][i]] * Hr[loco_perm_classic[0][i]];
					aa[i][5] = 1.0;
				}

				coeffs = imsl_d_lin_least_squares_gen((2 * SP + 1)*(2 * SP + 1), 6, &aa[0][0], &bb[0], IMSL_BASIS, tol, &kbasis, 0);

				///* Print results */
				fprintf(fp, "%lf  %lf  %lf\n", H[j] * H_norm, Hr[j] * H_norm, -coeffs[4]);

				//imsl_free(kbasis);
			}
		}
#else
		{
			// PRELUNGIRE
			for (i = 0; i < NFORC; i++)
			{
				for (j = 0; j < NFORC; j++)
				{
					if ((i < SP) || (i >= (NFORC - 1 - SP)) || (j < SP) || (j >= (NFORC - 1 - SP)))
					{
						fprintf(fp, "%lf  %lf  %lf\n", H_p[i*NFORC + j] * H_norm, Hr_p[i*NFORC + j] * H_norm, 0.0);
					}
					else
					{
						contor = 0;
						for (k = 0; k < (2 * SP + 1); k++)
						{
							for (l = 0; l < (2 * SP + 1); l++)
							{
								bb[contor] = M_p[(i - SP + k)*NFORC + j - SP + l];
								aa[contor][0] = Hr_p[(i - SP + k)*NFORC + j - SP + l];
								aa[contor][1] = H_p[(i - SP + k)*NFORC + j - SP + l];
								aa[contor][2] = Hr_p[(i - SP + k)*NFORC + j - SP + l] * Hr_p[(i - SP + k)*NFORC + j - SP + l];
								aa[contor][3] = H_p[(i - SP + k)*NFORC + j - SP + l] * H_p[(i - SP + k)*NFORC + j - SP + l];
								aa[contor][4] = H_p[(i - SP + k)*NFORC + j - SP + l] * Hr_p[(i - SP + k)*NFORC + j - SP + l];
								aa[contor][5] = 1.0;

								contor++;
							}
						}

						coeffs = imsl_d_lin_least_squares_gen((2 * SP + 1)*(2 * SP + 1), 6, &aa[0][0], &bb[0], IMSL_BASIS, tol, &kbasis, 0);

						///* Print results */
						fprintf(fp, "%lf  %lf  %lf\n", H_p[i*NFORC + j] * H_norm, Hr_p[i*NFORC + j] * H_norm, -coeffs[4]);


						if (H_p[i*NFORC + j] >= Hr_p[i*NFORC + j])
						{
							fprintf_s(fp_FORCcolo, "%20.16lf   %20.16lf   %20.16lf\n", H_p[i*NFORC + j] * H_norm, M_p[i*NFORC + j] * M_norm, -coeffs[4]);
						}

						//imsl_free(kbasis);
					}
				}
				fprintf_s(fp_FORCcolo, "\n");
			}
		}
#endif
		fclose(fp);
		fclose(fp_FORCcolo);
	}
#endif
	////////////////////////////////////////////////////////////////////////////
	////// FORC LOESS
	////////////////////////////////////////////////////////////////////////////
#ifdef LOESS
	{
		fopen_s(&fp, nume_fisOUT_LOESS, "w");
		fopen_s(&fp45, nume_fisOUT_LOESS45, "w");
		//fopen_s(&fp_FORCcolo, nume_fisOUT_DRAW_colo, "w");

		printf("NDATE = %d \n\n", NDATE);

		int *loco_loco_perm = NULL;
		int **loco_perm = &(loco_loco_perm);

#ifdef LOESS_ON_ORIGIAL_DATA
		{

			for (i = 0; i < NFORC*NMAX_PER_FORC/*NOBS*/; i++)
			{
				xx[i*N_INDEP + 0] = Hr_pre[i];
				xx[i*N_INDEP + 1] = H_pre[i];
				xx[i*N_INDEP + 2] = Hr_pre[i] * Hr_pre[i];
				xx[i*N_INDEP + 3] = H_pre[i] * H_pre[i];
				xx[i*N_INDEP + 4] = Hr_pre[i] * H_pre[i];
			}

			for (int j = 0; j < NFORC*NMAX_PER_FORC/*NOBS*/; j++)
			{
				/* Find points */
				for (i = 0; i < NFORC*NMAX_PER_FORC/*NOBS*/; i++)
				{
					dist[i] = sqrt((H_pre[j] - H_pre[i])*(H_pre[j] - H_pre[i]) + (Hr_pre[j] - Hr_pre[i])*(Hr_pre[j] - Hr_pre[i]));
				}
				sorted_result = imsl_d_sort(NFORC*NMAX_PER_FORC/*NOBS*/, &dist[0], IMSL_ABSOLUTE, IMSL_PERMUTATION, loco_perm, 0);

				/* indicii sunt in premutations[0][0] ... permutations[0][NDATE-1] */
				/* deci dmax = dist[permutations[0][NDATE-1]] */

				/* Find weights */
				dmax = dist[loco_perm[0][NDATE - 1]];

				for (i = 0; i < NFORC*NMAX_PER_FORC/*NOBS*/; i++)
				{
					w[i] = W((dist[i] / dmax));
				}

				coeffs = imsls_d_regression(NFORC*NMAX_PER_FORC/*NOBS*/, N_INDEP, (double *)xx, M_pre, IMSLS_TOLERANCE, tol, IMSLS_RANK, &rank, IMSLS_WEIGHTS, w, 0);

				/* Print results */
				if (rank == 6)
				{
					//wrong;
					fprintf_s(fp, "%20.16lf   %20.16lf   %20.16lf\n", H_pre[j] * H_norm, Hr_pre[j] * H_norm, -coeffs[5]);
					fprintf_s(fp45, "%20.16lf   %20.16lf   %20.16lf\n", (H_pre[j] - Hr_pre[j])*H_norm / 2.0, (H_pre[j] + Hr_pre[j])*H_norm / 2.0, -coeffs[5]);

					//if (H_pre[j] >= Hr_pre[j])
					//{
					//	fprintf_s(fp_FORCcolo, "%20.16lf   %20.16lf   %20.16lf\n", H_pre[j] * H_norm, M_pre[j] * M_norm, -coeffs[5]);
					//}
					//if ((j>0) && (Hr_pre[j] < Hr_pre[j - 1]))
					//{
					//	fprintf_s(fp_FORCcolo, "\n");
					//}

				}
				else
					wrong++;

				imsl_free(sorted_result);
				imsls_free(coeffs);

				if (j % 1000 == 0)
				{
					printf("DONE %5d \t wrong: %5d\n", j, wrong);
				}

			}
		}
#else
		{
			for (i = 0; i < NFORC*NFORC; i++)
			{
				xx_p[i*N_INDEP + 0] = Hr_p[i];
				xx_p[i*N_INDEP + 1] = H_p[i];
				xx_p[i*N_INDEP + 2] = Hr_p[i] * Hr_p[i];
				xx_p[i*N_INDEP + 3] = H_p[i] * H_p[i];
				xx_p[i*N_INDEP + 4] = Hr_p[i] * H_p[i];
			}

			for (int j = 0; j < NFORC*NFORC; j++)
			{
				/* Find points */
				for (i = 0; i < NFORC*NFORC; i++)
				{
					dist_p[i] = sqrt((H_p[j] - H_p[i])*(H_p[j] - H_p[i]) + (Hr_p[j] - Hr_p[i])*(Hr_p[j] - Hr_p[i]));
				}
				sorted_result = imsl_d_sort(NFORC*NFORC, &dist_p[0], IMSL_ABSOLUTE, IMSL_PERMUTATION, loco_perm, 0);

				/* indicii sunt in premutations[0][0] ... permutations[0][NDATE-1] */
				/* deci dmax = dist[permutations[0][NDATE-1]] */

				/* Find weights */
				dmax = dist_p[loco_perm[0][NDATE - 1]];

				for (i = 0; i < NFORC*NFORC; i++)
				{
					w_p[i] = W((dist_p[i] / dmax));
				}

				coeffs = imsls_d_regression(NFORC*NFORC, N_INDEP, xx_p, M_p, IMSLS_TOLERANCE, tol, IMSLS_RANK, &rank, IMSLS_WEIGHTS, w_p, 0);

				/* Print results */
				if (rank == 6)
				{
					if (H_p[j] >= Hr_p[j])
					{
						//not wrong;
						fprintf_s(fp, "%20.16lf   %20.16lf   %20.16lf\n", H_p[j] * H_norm, Hr_p[j] * H_norm, -coeffs[5]);
						fprintf_s(fp45, "%20.16lf   %20.16lf   %20.16lf\n", (H_p[j] - Hr_p[j])*H_norm / 2.0, (H_p[j] + Hr_p[j])*H_norm / 2.0, -coeffs[5]);

						if ((j > 0) && (Hr_p[j] != Hr_p[j - 1]))
							fprintf_s(fp_FORCcolo, "\n");
						fprintf_s(fp_FORCcolo, "%20.16lf   %20.16lf   %20.16lf\n", H_p[j] * H_norm, M_p[j] * M_norm, -coeffs[5]);
					}
					else
					{
						//not wrong, alt cadran;
						fprintf_s(fp, "%20.16lf   %20.16lf   %20.16lf\n", H_p[j] * H_norm, Hr_p[j] * H_norm, 0.0);
						fprintf_s(fp45, "%20.16lf   %20.16lf   %20.16lf\n", (H_p[j] - Hr_p[j])*H_norm / 2.0, (H_p[j] + Hr_p[j])*H_norm / 2.0, 0.0);
					}
				}
				else
					wrong++;

				imsl_free(sorted_result);
				imsls_free(coeffs);

				if (j % 1000 == 0)
				{
					printf("DONE %5d\n", j);
				}
			}
		}
#endif

		fclose(fp);
		fclose(fp45);
		//fclose(fp_FORCcolo);

		printf("NDATE = %d \nwrong = %d\n", NDATE, wrong);
	}
#endif

	free(H); free(Hr); free(M);
	free(H_pre); free(Hr_pre); free(M_pre);
	free(H_p); free(Hr_p); free(M_p);
	//free(w); 
	free(xx); 
	free(dist);
	free(w_p); 
	free(xx_p); 
	free(dist_p);
	free(Nr_pct_per_FORC);

	free(M_SMOOTH); free(xcoord); free(ycoord);

	return 0;
}

double W(double x)
{
	if (fabs(x) < 1.0)
		return((1.0 - x*x)*(1.0 - x*x));
	else
		return(0.0);
}


